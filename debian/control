Source: libhtml-tokeparser-simple-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: libhtml-parser-perl <!nocheck>,
                     libsub-override-perl <!nocheck>,
                     libwww-perl <!nocheck>,
                     perl
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libhtml-tokeparser-simple-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libhtml-tokeparser-simple-perl.git
Homepage: https://metacpan.org/release/HTML-TokeParser-Simple
Rules-Requires-Root: no

Package: libhtml-tokeparser-simple-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libhtml-parser-perl,
         libwww-perl
Description: Perl module used to tokenize HTML documents
 HTML::TokeParser::Simple is a subclass of HTML::TokeParser that uses
 easy-to-remember method calls to work with tokens.  For instance,
 rather than writing code like this:
 .
    $token->[0] eq 'S' and $token->[1] eq 'form' .
 .
 you can instead write something simpler like this:
 .
    $token->is_start_tag( 'form' )
