libhtml-tokeparser-simple-perl (3.16-4+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Fri, 28 Apr 2023 07:38:44 +0530

libhtml-tokeparser-simple-perl (3.16-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on libmodule-build-perl.
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Add libwww-perl to Build-Depends-Indep and Depends.
    Thanks to Felix Lechner for the bug report. (Closes: #1001164)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sun, 05 Dec 2021 17:21:43 +0100

libhtml-tokeparser-simple-perl (3.16-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Dominic Hargreaves ]
  * Update Standards-Version

 -- Dominic Hargreaves <dom@earth.li>  Sat, 28 Nov 2020 21:25:53 +0000

libhtml-tokeparser-simple-perl (3.16-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Sun, 31 May 2015 13:38:45 +0200

libhtml-tokeparser-simple-perl (3.16-1) unstable; urgency=low

  * New upstream release.
  * Drop patches, merged upstream.
  * Build-Depend on Module::Build 0.40.
  * Remove libtest-*perl, tests not run by default anymore.

 -- gregor herrmann <gregoa@debian.org>  Wed, 26 Jun 2013 21:32:32 +0200

libhtml-tokeparser-simple-perl (3.15-3) unstable; urgency=low

  * Add patch to fix POD errors. (Closes: #709829)

 -- gregor herrmann <gregoa@debian.org>  Sun, 26 May 2013 20:04:47 +0200

libhtml-tokeparser-simple-perl (3.15-2) unstable; urgency=low

  * Take over for the Debian Perl Group with maintainer's permission
    (https://lists.debian.org/debian-perl/2013/03/msg00034.html)
  * debian/control: Added: Vcs-Git field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Changed: Maintainer set to Debian
    Perl Group <pkg-perl-maintainers@lists.alioth.debian.org> (was:
    Christian Sánchez <csanchez@unplug.org.ve>); Christian Sánchez
    <csanchez@unplug.org.ve> moved to Uploaders.
  * debian/watch: use metacpan-based URL.

  * Switch to "3.0 (quilt)" source format.
  * Remove uupdate call from debian/watch.
  * Use tiny debian/rules. Don't install README anymore.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Remove versions from (build) dependencies that are not needed anymore.
  * Bump debhelper compatibility level to 8.
  * Set Standards-Version to 3.9.4 (no changes).
  * Add /me to Uploaders.
  * Enable more tests.
  * Add a patch to fix some spelling mistakes in the POD.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Mar 2013 13:36:28 +0100

libhtml-tokeparser-simple-perl (3.15-1) unstable; urgency=low

  * New upstream release

 -- Christian Sánchez <csanchez@unplug.org.ve>  Sat,  4 Mar 2006 21:26:48 +0000

libhtml-tokeparser-simple-perl (3.13-3) unstable; urgency=low

  *New maintainer. Closes(#331105)
   - control
       + Bumped Standards-Version to 3.6.2 (no changes).

 -- Christian Sánchez <csanchez@unplug.org.ve>  Sat, 24 Dec 2005 12:19:55 -0400

libhtml-tokeparser-simple-perl (3.13-2) unstable; urgency=low

  * Orphaning package.
  * Changed maintainer to "Debian QA Group" per standard procedure.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri,  7 Oct 2005 11:59:31 -0500

libhtml-tokeparser-simple-perl (3.13-1) unstable; urgency=low

  * New upstream release (skipped a few due to sarge freeze).
  * Added build dependency on libsub-override-perl.
  * Bumped standards version to 3.6.1 (no package changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 14 Oct 2004 20:05:52 -0500

libhtml-tokeparser-simple-perl (2.2-1) unstable; urgency=low

  * Initial release (closes: #244264).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 17 Apr 2004 11:28:02 -0500
